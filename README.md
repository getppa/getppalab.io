> A Personal Package Archive (PPA) is a software repository for uploading source packages to be built and published as an Advanced Packaging Tool (APT) repository by Launchpad.
> While the term is used exclusively within Ubuntu, Launchpad host Canonical envisions adoption beyond the Ubuntu community. [Learn More](https://en.wikipedia.org/wiki/Ubuntu_(operating_system)#Package_Archives)

## Git

```bash
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install git git-core git-all
```

## Subversion

```bash
sudo add-apt-repository ppa:dominik-stadler/subversion-1.9
sudo apt update
sudo apt install subversion subversion-tools
```

## Mercurial

```bash
sudo add-apt-repository ppa:mercurial-ppa/releases
sudo apt update
sudo apt install mercurial
```

## Ruby

```bash
sudo add-apt-repository ppa:brightbox/ruby-ng
sudo apt install ruby ruby-dev
```

## Java

```bash
sudo add-apt-repository ppa:webupd8team/java
sudo apt install oracle-java8-installer
```

## Redis

```bash
sudo add-apt-repository ppa:chris-lea/redis-server
sudo apt install redis-server
```
